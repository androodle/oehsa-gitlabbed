<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local
 * @subpackage pwf
 * @copyright  2015 onwards Androgogic Pty Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['advisor'] = 'SA Advisor';
$string['api_key'] = 'API key';
$string['company'] = 'Company';
$string['error_apikey_empty'] = 'API key is not set.';
$string['error_apikeytest_empty'] = 'Test API key is not set.';
$string['error_org_framework_empty'] = 'Organisation framework is not set.';
$string['error_password_empty'] = 'PWF password is not set.';
$string['error_passwordtest_empty'] = 'PWF test password is not set.';
$string['error_username_empty'] = 'PWF username is not set.';
$string['error_usernametest_empty'] = 'PWF test username is not set.';
$string['hoursused'] = 'Hours used';
$string['invalidorgcode'] = 'Organisation with this code not found. Please double check the code you entered.';
$string['livesettings'] = 'Live settings';
$string['membership'] = 'Membership';
$string['missingorgcode'] = 'Missing organisation code';
$string['off'] = 'Off';
$string['on'] = 'On';
$string['orgcode'] = 'Organisation code';
$string['org_framework'] = 'Organisations framework';
$string['org_framework_info'] = 'A framework where all ProWorkflow companies will belong to.';
$string['password'] = 'Password';
$string['password_info'] = 'ProWorkflow password for the username above.';
$string['pluginname'] = 'ProWorkflow';
$string['recognition'] = 'Recognition';
$string['sector'] = 'Sector';
$string['testmode'] = 'Test mode';
$string['testmode_info'] = 'Enable/disable test mode. Different sets of credentials will be used depending on this (see below).';
$string['testsettings'] = 'Test settings';
$string['username'] = 'Username';
$string['username_info'] = 'ProWorkflow username.';
