<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local
 * @subpackage pwf
 * @copyright  2015 onwards Androgogic Pty Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_pwf;

class pwf
{
    const LEVEL_INFO = 0;
    const LEVEL_WARNING = 1;
    const LEVEL_ERROR = 1;
    
    private $api_key;
    private $url = 'https://api.proworkflow.net';
    private $username;
    private $password;
    private $org_framework;
    
    public function __construct() 
    {
        $config = get_config('local_pwf');
        try {
            $this->test_config($config);
        } catch (pwf_exception $ex) {
            throw $ex;
        }
        
        $this->load_config($config);
    }
    
    public static function load_all_user_data($userid)
    {
        global $DB;
        
        $user = $DB->get_record('pwf_contacts', array('id' => $userid));
        if (empty($user)) {
            return false;
        }
        
        $user->company = $DB->get_record('pwf_companies', array('id' => $user->companyid));
        
        return $user;
    }
    
    public static function log($level, $info)
    {
        global $DB;
        
        $data = new \stdClass();
        $data->level = $level;
        $data->info = $info;
        $data->created = time();

        try {
            return $DB->insert_record('pwf_import_log', $data);
        } catch (\Exception $ex) {
            throw new pwf_exception($ex->getMessage() ."\n". $ex->debuginfo);
        }
            
    }
    
    private function test_config(&$config)
    {
        if (empty($config->org_framework)) {
            throw new pwf_exception(get_string('error_org_framework_empty', 'local_pwf'));
        }
        if (!empty($config->testmode)) {
            if (empty($config->api_key_test)) {
                throw new pwf_exception(get_string('error_apikeytest_empty', 'local_pwf'));
            }
            if (empty($config->username_test)) {
                throw new pwf_exception(get_string('error_usernametest_empty', 'local_pwf'));
            }
            if (empty($config->password_test)) {
                throw new pwf_exception(get_string('error_passwordtest_empty', 'local_pwf'));
            }
        } else {
            if (empty($config->api_key)) {
                throw new pwf_exception(get_string('error_apikey_empty', 'local_pwf'));
            }
            if (empty($config->username)) {
                throw new pwf_exception(get_string('error_username_empty', 'local_pwf'));
            }
            if (empty($config->password)) {
                throw new pwf_exception(get_string('error_password_empty', 'local_pwf'));
            }
        }
    }
    
    private function load_config(&$config)
    {
        $this->api_key = ($config->testmode) ? $config->api_key_test : $config->api_key;
        $this->username = ($config->testmode) ? $config->username_test : $config->username;
        $this->password = ($config->testmode) ? $config->password_test : $config->password;
        $this->org_framework = $config->org_framework;
    }
    
    private function send_request($command, $params = array())
    {
		$headers = array(
			'Connection: Keep-Alive',
			'User-Agent: Totara/4.1.1 (java 1.5)',
			'apikey: ' . $this->api_key,
		);
        
        $url = $this->url . '/' . $command;
        if (!empty($params)) {
            $url .= '?';
            foreach ($params as $key => $value) {
                $url .= '&'.$key.'='.$value;
            }
        }

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_USERPWD, $this->username.':'.$this->password);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($curl, CURLOPT_TIMEOUT, 20);

		$response = curl_exec($curl);
        if (curl_errno($curl)) {
            throw new pwf_exception('cURL error: ' . curl_error($curl));
        }

		$obj = json_decode($response);
        if ($obj === null) {
            throw new pwf_exception('PWF response is invalid.');
        }   
        
        if ($obj->status === 'Error') {
            throw new pwf_exception(implode('. ', $obj->details));
        }
        
        return $obj;
    }
    
    public function sync_organisations() 
    {
        $params = array('fields' => 'code,name,type,tags,image');
        try {
            $result = $this->send_request('companies', $params);
        } catch (pwf_exception $ex) {
            throw $ex;
        }
        
        if (empty($result->companies)) {
            return;
        }
        
        try {
            $this->store_organisations($result->companies);
        } catch (pwf_exception $ex) {
            throw $ex;
        }
        
        try {
            $this->import_organisations();
        } catch (pwf_exception $ex) {
            throw $ex;
        }
    }
    
    public function sync_projects() 
    {
        $params = array(
            'fields' => 'company,title,startdate,duedate,timeallocated'
        );
        
        try {
            $result = $this->send_request('projects', $params);
        } catch (pwf_exception $ex) {
            throw $ex;
        }
        
        if (empty($result->projects)) {
            return;
        }
        
        try {
            $this->store_project_data($result->projects);
        } catch (pwf_exception $ex) {
            throw $ex;
        }
    }
    
    private function timestamp_of($date)
    {
        $dt = new \DateTime($date);
        return $dt->getTimestamp();
    }
    
    private function store_project_data($projects)
    {
        global $DB;
        
        if (empty($projects)) {
            return;
        }
        
        foreach ($projects as $project) 
        {
            // Process only projects which have "member management" in its title.
            if (strpos($project->title, 'member management') === false) {
                continue;
            }
            
            $data = new \stdClass;
            $data->id = $project->companyid;
            $data->taskstimeallocated = $project->taskstimeallocated;
            $data->startdate = $this->timestamp_of($project->startdate);
            $data->duedate = $this->timestamp_of($project->duedate);
            
            try {
                $DB->update_record('pwf_companies', $data);
            } catch (\Exception $ex) {
                throw new pwf_exception($ex->getMessage() ."\n". $ex->debuginfo);
            }
        }
    }
    
    private function store_organisations($companies)
    {
        global $DB;
        $DB->delete_records('pwf_companies');
        if (empty($companies)) {
            return;
        }
        foreach ($companies as $company) {
            try {
                $DB->import_record('pwf_companies', $this->build_company_object($company));
            } catch (\Exception $ex) {
                throw new pwf_exception($ex->getMessage() ."\n". $ex->debuginfo);
            }
        }
    }
    
    private function build_company_object(&$company)
    {
        $data = new \stdClass();
        $data->id = $company->id;
        $data->code = trim($company->code);
        $data->name = trim($company->name);
        $data->type = trim($company->type);
        $data->logo = trim($company->image);
        $data->startdate = 0;
        $data->duedate = 0;
        $data->taskstimeallocated = 0;
        $data->sector = $this->find_tag($company, 'sector');
        $data->badge = $this->find_tag($company, 'badge');
        $data->advisor = $this->find_tag($company, 'advisor');
        return $data;
    }
    
    private function find_tag(&$company, $tagname)
    {
        if (empty($company->tags)) {
            return '';
        }
        foreach ($company->tags as $tag) {
            if ($tagname == 'sector' && substr($tag->name, 0, 3) == 'S: ') {
                return str_replace('S: ', '', $tag->name);
            } elseif ($tagname == 'advisor' && substr($tag->name, 0, 4) == 'PO: ') {
                return str_replace('PO: ', '', $tag->name);
            } elseif ($tagname == 'badge') {
                switch ($tag->name) {
                    case '1. Member': return 'Member';
                    case '2. Bronze': return 'Bronze';
                    case '3. Silver': return 'Silver';
                    case '4. Gold': return 'Gold';
                }
            }
        }
        return '';
    }
    
    private function store_users($users)
    {
        global $DB;
        if (empty($users)) {
            return;
        }
        $DB->delete_records('pwf_contacts');
        foreach ($users as $user) {
            try {
                $DB->import_record('pwf_contacts', $user);
            } catch (\Exception $ex) {
                throw new pwf_exception($ex->getMessage() ."\n". $ex->debuginfo);
            }
        }
    }
    
    private function import_organisations()
    {
        global $DB;
        
        $companies = $DB->get_records('pwf_companies');
        if (empty($companies)) {
            return;
        }
        
        foreach ($companies as $company) {
            $id = $this->get_organisation_id($company->id);
            if (empty($id)) {
                try {
                    $this->import_organisation($company);
                } catch (pwf_exception $ex) {
                    throw $ex;
                }
            } else {
                $this->update_organisation($id, $company);
            }
        }
    }
    
    private function import_users()
    {
        global $DB;
        
        $users = $DB->get_records('pwf_contacts');
        if (empty($users)) {
            return;
        }
        
        foreach ($users as $user) {
            $id = $this->get_user_id($user);
            if (empty($id)) {
                try {
                    $id = $this->import_user($user);
                } catch (pwf_exception $ex) {
                    throw $ex;
                }
            } else {
                $this->update_user($id, $user);
            }
            
            try {
                $this->assign_user_to_organisation($id, $user->companyid, $user->title, $user->type);
            } catch (pwf_exception $ex) {
                throw $ex;
            }
        }
    }
    
    private function assign_user_to_organisation($userid, $companyid, $position, $usertype)
    {
        global $DB;
        
        $orgid = $this->get_organisation_id($companyid);
        if (empty($orgid) || empty($userid) || empty($position)) {
            return;
        }
        
        // Check if user is already assigned to this organisation.
        $id = $this->get_position_id($userid, $orgid, $usertype);
        if (!empty($id)) {
            return;
        }
        
        try {
            self::save_position($position, $usertype, $orgid, $userid);
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public static function get_orgid_from_code($code) 
    {
        global $DB;
        $sql = "SELECT o.id
            FROM {pwf_companies} pc
            LEFT JOIN {org} o ON o.idnumber = pc.id
            WHERE pc.code = ?";
        return $DB->get_field_sql($sql, array($code));
    }
    
    public static function save_position($orgid, $userid, $position = '', $usertype = NULL)
    {
        global $DB;
        
        // Clear all other assignments.
        $DB->delete_records('pos_assignment', array(
            'userid' => $userid,
            'organisationid' => $orgid,
        ));
        
        $data = new \stdClass();
        $data->fullname = $position;
        $data->shortname = $usertype;
        $data->organisationid = $orgid;
        $data->userid = $userid;
        $data->timecreated = time();
        $data->timemodified = time();
        $data->usermodified = 0;
        
        $sql = "SELECT MAX(type) AS m FROM {pos_assignment} WHERE userid = ?";
        $result = $DB->get_record_sql($sql, array($userid));
        if (!empty($result)) {
            $data->type = $result->m + 1;
        } else {
            $data->type = 1;
        }
        
        try {
            $DB->insert_record('pos_assignment', $data);
        } catch (\Exception $ex) {
            throw new pwf_exception($ex->getMessage() ."\n". $ex->debuginfo);
        }
    }
    
    private function get_organisation_id($companyid) 
    {
        global $DB;
        $params = array(
            'frameworkid' => $this->org_framework,
            'idnumber' => $companyid,
        );
        return $DB->get_field('org', 'id', $params);
    }
    
    private function get_position_id($userid, $orgid, $usertype) 
    {
        global $DB;
        $params = array(
            'organisationid' => $orgid,
            'userid' => $userid,
            'shortname' => $usertype,
        );
        return $DB->get_field('pos_assignment', 'id', $params);
    }
    
    private function get_user_id(&$data) 
    {
        // Find by PWF ID, then by email
        global $DB;
        $params = array(
            'idnumber' => $data->id,
        );
        $id = $DB->get_field('user', 'id', $params);
        if (!empty($id)) {
            return $id;
        }
        $params = array(
            'email' => $data->email,
        );
        return $DB->get_field('user', 'id', $params);
    }
    
    private function import_organisation($data) 
    {
        global $DB;
        
        $org = new \stdClass();
        $org->idnumber = $data->id;
        $org->parentid = 0;
        $org->fullname = $data->name;
        $org->frameworkid = $this->org_framework;
        $org->timecreated = time();
        $org->timemodified = time();
        $org->usermodified = 0;
        $org->depthlevel = 1;
        $org->typeid = 0;
        $org->visible = 1;
        
        try {
            $DB->insert_record('org', $org);
        } catch (\Exception $ex) {
            throw new pwf_exception($ex->getMessage() ."\n". $ex->debuginfo);
        }
        
        $this->log(self::LEVEL_INFO, 'Organisation "' . $data->name . '" imported');
    }
    
    private function import_user($data) 
    {
        global $DB;
        
        $user = $this->build_user_object($data);
        try {
            $id = $DB->insert_record('user', $user);
        } catch (\Exception $ex) {
            throw new pwf_exception($ex->getMessage() ."\n". $ex->debuginfo);
        }
        
        $this->log(self::LEVEL_INFO, 'User "' . $data->firstname .' '. $data->lastname . '" ('.$data->id.') imported');
        
        return $id;
    }
    
    private function build_user_object(&$data, $id = null)
    {
        $user = new \stdClass();

        if (!empty($id)) {
            $user->id = $id;
        } else {
            $user->auth = 'manual';
            $user->password = hash_internal_user_password(generate_password());
            $user->timecreated = time();
        }
        $user->idnumber = $data->id;
        $user->firstname = $data->firstname;
        $user->lastname = $data->lastname;
        $user->email = $this->get_user_email($data);
        $user->username = $user->email;
        $user->timemodified = time();
        $user->address = implode(' ', array(
            $data->address1, 
            $data->address2,
            $data->address3
        ));
        $user->phone1 = substr($data->mobilephone, 0, 20);
        $user->phone2 = substr($data->workphone, 0, 20);
        
        return $user;
    }
    
    private function update_organisation($id, $data) 
    {
        global $DB;
        
        $org = new \stdClass();
        $org->id = $id;
        $org->idnumber = $data->id;
        $org->fullname = $data->name;
        $org->timemodified = time();
        $org->usermodified = 0;
        
        try {
            $DB->update_record('org', $org);
        } catch (\Exception $ex) {
            throw new pwf_exception($ex->getMessage() ."\n". $ex->debuginfo);
        }
        
        $this->log(self::LEVEL_INFO, 'Organisation "' . $data->name . '" updated');
    }
    
    private function get_user_email(&$data) 
    {
        if ($data->email != 'noreply@proworkflow.com') {
            return $data->email;
        }
        return 'u'.$data->id.'@sustainability.environment.nsw.gov.au';
    }
    
    private function update_user($id, $data) 
    {
        global $DB;
        
        $user = $this->build_user_object($data, $id);
        try {
            $DB->update_record('user', $user);
        } catch (\Exception $ex) {
            throw new pwf_exception($ex->getMessage() ."\n". $ex->debuginfo);
        }
        
        $this->log(self::LEVEL_INFO, 'User "' . $data->firstname .' '. $data->lastname . '" ('.$data->id.') updated');
        
        return $id;
    }
    
    public function sync_users() 
    {
        $params = array(
            'fields' => 'name,company,type,email,phone,address,title',
        );
        try {
            $result = $this->send_request('contacts', $params);
        } catch (pwf_exception $ex) {
            throw $ex;
        }
        
        if (empty($result->contacts)) {
            return;
        }

        try {
            $this->store_users($result->contacts);
        } catch (pwf_exception $ex) {
            throw $ex;
        }
        
        try {
            $this->import_users();
        } catch (pwf_exception $ex) {
            throw $ex;
        }
    }
    
    public static function get_advisor_data($fullname)
    {
        global $DB;
        
        if (empty($fullname)) {
            return false;
        }
        
        $sql = "SELECT phone1, email
            FROM {user}
            WHERE ".$DB->sql_concat('firstname', "' '", 'lastname')." = :fullname";
        $params['fullname'] = $fullname;
        
        $results = $DB->get_records_sql($sql, $params);
        if (!$results) {
            return false;
        }
        
        $advisor = new \stdClass();
        $advisor->phone = 'N/A';
        $advisor->email = 'N/A';
        foreach ($results as $result) {
            if (!empty($result->phone1)) {
                $advisor->phone = $result->phone1;
            }
            if (!empty($result->email)) {
                $advisor->email = $result->email;
            }
        }
        
        return $advisor;
    }
}

class pwf_exception extends \Exception
{
    public function __construct($message, $code = 0, Exception $previous = null) 
    {
        parent::__construct($message, $code, $previous);
        pwf::log(pwf::LEVEL_ERROR, $message);
    }
}
