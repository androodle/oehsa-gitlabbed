<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local
 * @subpackage pwf
 * @copyright  2015 onwards Androgogic Pty Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once("{$CFG->dirroot}/totara/hierarchy/lib.php");
require_once("{$CFG->dirroot}/local/pwf/lib.php");

if ($ADMIN->locate('localplugins')) {
    
    $settings = new admin_settingpage('localpwf', get_string('pluginname', 'local_pwf'));

	$settings->add(new admin_setting_configselect(
		'local_pwf/testmode',
        get_string('testmode', 'local_pwf'), 
		get_string('testmode_info', 'local_pwf'), 
		0, 
		array(
            0 => get_string('off', 'local_pwf'), 
            1 => get_string('on', 'local_pwf')
        )
	));

    $frameworks = local_pwf_get_frameworks();
    $frameworks[0] = '';  // adds an empty element to the top of the list
    ksort($frameworks);
    $settings->add(new admin_setting_configselect(
		'local_pwf/org_framework',
        get_string('org_framework', 'local_pwf'), 
		get_string('org_framework_info', 'local_pwf'), 
		0, 
		$frameworks
	));
    
    $settings->add(new admin_setting_heading(
        'livesettings', 
        get_string('livesettings', 'local_pwf'),
        null
    ));

	$settings->add(new admin_setting_configtext(
		'local_pwf/api_key',
        get_string('api_key', 'local_pwf'), 
		'', 
		'', 
		PARAM_RAW_TRIMMED,
		50
	));

	$settings->add(new admin_setting_configtext(
		'local_pwf/username',
        get_string('username', 'local_pwf'), 
		get_string('username_info', 'local_pwf'), 
		'', 
		PARAM_RAW_TRIMMED,
		20
	));

	$settings->add(new admin_setting_configpasswordunmask(
		'local_pwf/password',
        get_string('password', 'local_pwf'), 
		get_string('password_info', 'local_pwf'), 
		'', 
		PARAM_RAW_TRIMMED,
		20
	));
    
    $settings->add(new admin_setting_heading(
        'testsettings', 
        get_string('testsettings', 'local_pwf'),
        null
    ));

	$settings->add(new admin_setting_configtext(
		'local_pwf/api_key_test',
        get_string('api_key', 'local_pwf'), 
		'', 
		'', 
		PARAM_RAW_TRIMMED,
		50
	));

	$settings->add(new admin_setting_configtext(
		'local_pwf/username_test',
        get_string('username', 'local_pwf'), 
		get_string('username_info', 'local_pwf'), 
		'', 
		PARAM_RAW_TRIMMED,
		20
	));

	$settings->add(new admin_setting_configpasswordunmask(
		'local_pwf/password_test',
        get_string('password', 'local_pwf'), 
		get_string('password_info', 'local_pwf'), 
		'', 
		PARAM_RAW_TRIMMED,
		20
	));

    $ADMIN->add('localplugins', $settings);
}
