<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local
 * @subpackage pwf
 * @copyright  2015 onwards Androgogic Pty Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function local_pwf_cron() 
{
    mtrace("ProWorkflow sync started.");
    
    try {
        $pwf = new \local_pwf\pwf();
    } catch (Exception $ex) {
        mtrace('Sync aborted: '. $ex->getMessage());
        return;
    }
    
    try {
        mtrace('Syncing organisations...');
        $pwf->sync_organisations();
    } catch (Exception $ex) {
        mtrace('Sync aborted: '. $ex->getMessage());
        return;
    }
    
    try {
        mtrace('Syncing projects...');
        $pwf->sync_projects();
    } catch (Exception $ex) {
        mtrace('Sync aborted: '. $ex->getMessage());
        return;
    }
    
    try {
        mtrace('Syncing users...');
        $pwf->sync_users();
    } catch (Exception $ex) {
        mtrace('Sync aborted: '. $ex->getMessage());
        return;
    }
    
    mtrace("ProWorkflow sync ended.");
}

function local_pwf_get_frameworks()
{
    global $DB;
    return $DB->get_records_menu('org_framework', null, '', 'id, fullname');
}
