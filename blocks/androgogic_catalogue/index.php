<?php
/** 
 * Androgogic Catalogue Block: Index
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

require_once('../../config.php');
require_once('lib.php');
global $CFG,$USER,$DB,$PAGE, $OUTPUT;
//expected params
$currenttab = optional_param('tab', 'catalogue_entry_search', PARAM_TEXT);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_url("$CFG->wwwroot/blocks/androgogic_catalogue/index.php", array('tab'=>$currenttab));
$PAGE->set_title(get_string($currenttab, 'block_androgogic_catalogue'));
$PAGE->set_heading(get_string('plugintitle', 'block_androgogic_catalogue'));
$navigation = $PAGE->navbar->add(get_string('catalogue_entry_search','block_androgogic_catalogue'), "$CFG->wwwroot/blocks/androgogic_catalogue/index.php");
if($currenttab != 'catalogue_entry_search'){
    require_login();
}
echo $OUTPUT->header();
//show tabs
include('tabs.php');
// include current page
include $currenttab.'.php';
 echo $OUTPUT->footer();

// End of blocks/androgogic_catalogue/index.php