<?php
/** 
 * Androgogic Support Block: Class
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     06/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

class block_androgogic_support extends block_base {

	function init(){
		$this->title = get_string('plugintitle','block_androgogic_support');
		$this->cron = 1;
	} //end function init

	function get_content(){
		global $CFG;		if ($this->content !== null) {
			return $this->content;
		}
		$this->content = new stdClass;
                $this->content->text = '';
                $context = get_context_instance(CONTEXT_SYSTEM);
                if(has_capability('block/androgogic_support:edit', $context)){
                    $this->content->text .= '<a href="'.$CFG->wwwroot.'/blocks/androgogic_support/index.php">'.get_string('support_admin','block_androgogic_support').'</a><br>';
                }
                if(has_capability('block/androgogic_support:view', $context)){
                    $this->content->text .= '<a href="'.$CFG->wwwroot.'/blocks/androgogic_support/index.php?tab=server_status">'.get_string('support_log_new','block_androgogic_support').'</a><br>';
                }
		return $this->content;

	} //end function get_content

	function cron(){

	} //end function cron
        function has_config() {
            return true;
        }
} //end class block_androgogic_support

// End of blocks/androgogic_support/block_androgogic_support.php