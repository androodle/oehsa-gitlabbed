<?php

/** 
 * Androgogic Support Block: Search
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     07/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Search support_logs
 * Also provides access to edit and delete functions if user has sufficient permissions
 *
 **/

//params
$sort   = optional_param('sort', 'first_name', PARAM_RAW);
$dir    = optional_param('dir', 'ASC', PARAM_ALPHA);
$page   = optional_param('page', 0, PARAM_INT);
$perpage= optional_param('perpage', 20, PARAM_INT); 
$search = optional_param('search', '', PARAM_TEXT);
$tab    = optional_param('tab', 'support_log_search', PARAM_TEXT);
$user_id = optional_param('user_id', 0, PARAM_INT); 

require_capability('block/androgogic_support:edit', $context);

if (!is_siteadmin()) {
    error('Only for admins');
}

// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page','perpage','search','tab')); 
// prepare columns for results table
$columns = array(
"first_name",
"last_name",
"email",
"contact_number",
"problem_description",
"steps_to_reproduce",
"uploaded_file",
"site_name",
"page_before_support",
"user",
);
foreach ($columns as $column) {
$string[$column] = get_string("$column",'block_androgogic_support');
if ($sort != $column) {
$columnicon = '';
$columndir = 'ASC';
} else {
$columndir = $dir == 'ASC' ? 'DESC':'ASC';
$columnicon = $dir == 'ASC' ? 'down':'up';
}
if($column != 'details'){
$$column = "<a href='$PAGE->url&amp;dir=$columndir&amp;sort=$column'>$string[$column]</a>";
}
else{
 $$column = $string[$column];
}
}
//figure out the and clause from what has been submitted
$and = '';
if($search != ''){
$and .= " and concat(
a.first_name,
a.last_name,
a.email,
a.contact_number,
a.problem_description,
a.steps_to_reproduce,
a.site_name,
a.page_before_support
) like '%$search%'";
} 
//are we filtering on user?
if($user_id > 0){ 
$and .= " and mdl_user.id = $user_id ";
} 
$q = "select DISTINCT a.* , CONCAT(mdl_user.firstname,' ',mdl_user.lastname) as user, mdl_files.contextid, mdl_files.component, mdl_files.filearea, mdl_files.filename, mdl_files.itemid, mdl_files.id as fileid
from mdl_androgogic_support_log a
LEFT JOIN mdl_user on a.user_id = mdl_user.id
LEFT JOIN mdl_files on a.uploaded_file_id = mdl_files.itemid and mdl_files.itemid != 0 and mdl_files.filename != '.'
where 1 = 1 
$and 
order by $sort $dir";
if(isset($_GET['debug'])){echo '$query : ' . $q . ''   ;}
//get a page worth of records
$results = $DB->get_records_sql($q,array(),$page*$perpage, $perpage);
//also get the total number we have of these
$q = "SELECT COUNT(DISTINCT a.id)
from mdl_androgogic_support_log a 
LEFT JOIN mdl_user on a.user_id = mdl_user.id
LEFT JOIN mdl_files on a.uploaded_file_id = mdl_files.itemid and mdl_files.itemid != 0 and mdl_files.filename != '.'
 where 1 = 1  $and";

if(isset($_GET['debug'])){echo '$query : ' . $q . '<br>'   ;}

$result_count = $DB->get_field_sql($q);
require_once('support_log_search_form.php');
$mform = new support_log_search_form(null, array('sort' => $sort,'dir'=>$dir,'perpage'=>$perpage,'search'=>$search,'tab'=>$currenttab ,'user_id'=>$user_id));
$mform->display();
echo '<table width="100%"><tr><td width="50%">';
echo $result_count . ' ' . get_string('support_log_plural','block_androgogic_support') . " found" . '<br>';
echo '</td><td style="text-align:right;">';
if(has_capability('block/androgogic_support:edit', $context)){
echo "<a href='index.php?tab=support_log_new'>" . get_string('support_log_new','block_androgogic_support') . "</a>";
}
echo '</td></tr></table>';

flush();

//RESULTS
if (!$results) {
$match = array();
echo $OUTPUT->heading(get_string('noresults','block_androgogic_support',$search));
} else {
$table = new html_table();
$table->head = array (
$first_name,
$last_name,
$email,
$contact_number,
$problem_description,
$steps_to_reproduce,
$uploaded_file,
$site_name,
$page_before_support,
$user,
);
$table->align = array ("left","left","left","left","left","left","left","left","left","left","left","left",);
$table->width = "95%";
$table->size = array("8%","8%","8%","8%","8%","8%","8%","8%","8%","8%","8%","8%",);
foreach ($results as $result) {
$view_link = '';//enable if wanted: "<a href='index.php?tab=support_log_view&id=$result->id'>View</a> "; 
$edit_link = "";
$delete_link = "";
if(has_capability('block/androgogic_support:edit', $context)){
// then we show the edit link
$edit_link = "<a href='index.php?tab=support_log_edit&id=$result->id'>Edit</a> ";
}
if(has_capability('block/androgogic_support:delete', $context)){
// then we show the delete link
$delete_link = "<a href='index.php?tab=support_log_delete&id=$result->id' onclick='return confirm(\"Are you sure you want to delete this item?\")'>Delete</a> ";
}
//link for file
$file_link = '';
if(isset($result->filename)){
$file_link = new moodle_url("/pluginfile.php/{$result->contextid}/{$result->component}/{$result->filearea}/" . $result->itemid.'/'.$result->filename);
}
$table->data[] = array (
"$result->first_name",
"$result->last_name",
"$result->email",
"$result->contact_number",
"$result->problem_description",
"$result->steps_to_reproduce",
html_writer::link($file_link, $result->filename),
"$result->site_name",
"<a href='$result->page_before_support' target='_blank'>Last page before support</a>",
"$result->user",
$view_link . $edit_link . $delete_link
);
}
}
if (!empty($table)) {
echo html_writer::table($table);
$pagingbar = new paging_bar($result_count, $page, $perpage,$PAGE->url);
$pagingbar->pagevar = 'page';
echo $OUTPUT->render($pagingbar);
}


?>
