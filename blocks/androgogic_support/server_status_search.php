<?php

/** 
 * Androgogic Support Block: Search
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     06/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Search server_statuses
 * Also provides access to edit and delete functions if user has sufficient permissions
 *
 **/

//params
$sort   = optional_param('sort', 'name', PARAM_RAW);
$dir    = optional_param('dir', 'ASC', PARAM_ALPHA);
$page   = optional_param('page', 0, PARAM_INT);
$perpage= optional_param('perpage', 20, PARAM_INT); 
$search = optional_param('search', '', PARAM_TEXT);
$tab    = optional_param('tab', 'server_status_search', PARAM_TEXT);

require_capability('block/androgogic_support:view', $context);

// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page','perpage','search','tab')); 
//figure out the and clause from what has been submitted
$and = '';
if($search != ''){
$and .= " and concat(
a.name,
a.summary
) like '%$search%'";
} 
$q = "select DISTINCT a.*  
from mdl_androgogic_server_status a 
where 1 = 1 
$and 
order by $sort $dir";
if(isset($_GET['debug'])){echo '$query : ' . $q . ''   ;}
//get a page worth of records
$results = $DB->get_records_sql($q,array(),$page*$perpage, $perpage);
//also get the total number we have of these
$q = "SELECT COUNT(DISTINCT a.id)
from mdl_androgogic_server_status a 
 where 1 = 1  $and";

if(isset($_GET['debug'])){echo '$query : ' . $q . '<br>'   ;}

$result_count = $DB->get_field_sql($q);
require_once('server_status_search_form.php');
$mform = new server_status_search_form(null, array('sort' => $sort,'dir'=>$dir,'perpage'=>$perpage,'search'=>$search,'tab'=>$currenttab ));
$mform->display();
echo '<table width="100%"><tr><td width="50%">';
echo $result_count . ' ' . get_string('server_status_plural','block_androgogic_support') . " found" . '<br>';
echo '</td><td style="text-align:right;">';
if(has_capability('block/androgogic_support:edit', $context)){
echo "<a href='index.php?tab=server_status_new'>" . get_string('server_status_new','block_androgogic_support') . "</a>";
}
echo '</td></tr></table>';

flush();

//RESULTS
if (!$results) {
$match = array();
echo $OUTPUT->heading(get_string('noresults','block_androgogic_support',$search));
} else {
echo '<hr>';
foreach ($results as $result) {
echo '<h3>'. $result->name .'</h3>';
$edit_link = "";
$delete_link = "";
if(has_capability('block/androgogic_support:edit', $context)){
// then we show the edit link
$edit_link = "<a href='index.php?tab=server_status_edit&id=$result->id'>Edit</a> ";
}
if(has_capability('block/androgogic_support:delete', $context)){
// then we show the delete link
$delete_link = "<a href='index.php?tab=server_status_delete&id=$result->id' onclick='return confirm(\"Are you sure you want to delete this item?\")'>Delete</a> ";
}
echo $edit_link . $delete_link . '<br>';
echo format_text($result->summary,FORMAT_HTML) .'<br/>';
echo '<hr>';
$pagingbar = new paging_bar($result_count, $page, $perpage,$PAGE->url);
$pagingbar->pagevar = 'page';
echo $OUTPUT->render($pagingbar);
}
}

?>
