<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Public Profile -- a user's public profile page
 *
 * - each user can currently have their own page (cloned from system and then customised)
 * - users can add any blocks they want
 * - the administrators can define a default site public profile for users who have
 *   not created their own public profile
 *
 * This script implements the user's view of the public profile, and allows editing
 * of the public profile.
 *
 * @package    moodlecore
 * @subpackage my
 * @copyright  2010 Remote-Learner.net
 * @author     Hubert Chathi <hubert@remote-learner.net>
 * @author     Olav Jordan <olav.jordan@remote-learner.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../config.php');
require_once($CFG->dirroot . '/my/lib.php');
require_once($CFG->dirroot . '/tag/lib.php');
require_once($CFG->dirroot . '/user/profile/lib.php');
require_once($CFG->libdir.'/filelib.php');

$userid         = optional_param('id', 0, PARAM_INT);
$edit           = optional_param('edit', null, PARAM_BOOL);    // Turn editing on and off.
$showallcourses = optional_param('showallcourses', 0, PARAM_INT);

$PAGE->set_url('/user/profile.php', array('id'=>$userid));

if (!empty($CFG->forceloginforprofiles)) {
    require_login();
    if (isguestuser()) {
        $SESSION->wantsurl = $PAGE->url->out(false);
        redirect(get_login_url());
    }
} else if (!empty($CFG->forcelogin)) {
    require_login();
}

$userid = $userid ? $userid : $USER->id;       // Owner of the page
if ((!$user = $DB->get_record('user', array('id' => $userid))) || ($user->deleted)) {
    $PAGE->set_context(context_system::instance());
    echo $OUTPUT->header();
    if (!$user) {
        echo $OUTPUT->notification(get_string('invaliduser', 'error'));
    } else {
        echo $OUTPUT->notification(get_string('userdeleted'));
    }
    echo $OUTPUT->footer();
    die;
}

$currentuser = ($user->id == $USER->id);
$context = $usercontext = context_user::instance($userid, MUST_EXIST);

if (!$currentuser &&
    !empty($CFG->forceloginforprofiles) &&
    !has_capability('moodle/user:viewdetails', $context) &&
    !has_coursecontact_role($userid)) {

    // Course managers can be browsed at site level. If not forceloginforprofiles, allow access (bug #4366)
    $struser = get_string('user');
    $PAGE->set_context(context_system::instance());
    $PAGE->set_title("$SITE->shortname: $struser");  // Do not leak the name
    $PAGE->set_heading("$SITE->shortname: $struser");
    $PAGE->set_url('/user/profile.php', array('id'=>$userid));
    $PAGE->navbar->add($struser);
    echo $OUTPUT->header();
    echo $OUTPUT->notification(get_string('usernotavailable', 'error'));
    echo $OUTPUT->footer();
    exit;
}

// Get the profile page.  Should always return something unless the database is broken.
if (!$currentpage = my_get_page($userid, MY_PAGE_PUBLIC)) {
    print_error('mymoodlesetup');
}

if (!$currentpage->userid) {
    $context = context_system::instance();  // A trick so that we even see non-sticky blocks
}

$PAGE->set_context($context);
$PAGE->set_pagelayout('mypublic');
$PAGE->set_pagetype('user-profile');

// Set up block editing capabilities
if (isguestuser()) {     // Guests can never edit their profile
    $USER->editing = $edit = 0;  // Just in case
    $PAGE->set_blocks_editing_capability('moodle/my:configsyspages');  // unlikely :)
} else {
    if ($currentuser) {
        $PAGE->set_blocks_editing_capability('moodle/user:manageownblocks');
    } else {
        $PAGE->set_blocks_editing_capability('moodle/user:manageblocks');
    }
}

if (has_capability('moodle/user:viewhiddendetails', $context)) {
    $hiddenfields = array();
} else {
    $hiddenfields = array_flip(explode(',', $CFG->hiddenuserfields));
}

if (has_capability('moodle/site:viewuseridentity', $context)) {
    $identityfields = array_flip(explode(',', $CFG->showuseridentity));
} else {
    $identityfields = array();
}

// Start setting up the page
$strpublicprofile = get_string('publicprofile');

$PAGE->blocks->add_region('content');
$PAGE->set_subpage($currentpage->id);
$PAGE->set_title(fullname($user).": $strpublicprofile");
$PAGE->set_heading(fullname($user).": $strpublicprofile");

if (!$currentuser) {
    $PAGE->navigation->extend_for_user($user);
    if ($node = $PAGE->settingsnav->get('userviewingsettings'.$user->id)) {
        $node->forceopen = true;
    }
} else if ($node = $PAGE->settingsnav->get('usercurrentsettings', navigation_node::TYPE_CONTAINER)) {
    $node->forceopen = true;
}
if ($node = $PAGE->settingsnav->get('root')) {
    $node->forceopen = false;
}


// Toggle the editing state and switches
if ($PAGE->user_allowed_editing()) {
    if ($edit !== null) {             // Editing state was specified
        $USER->editing = $edit;       // Change editing state
        if (!$currentpage->userid && $edit) {
            // If we are viewing a system page as ordinary user, and the user turns
            // editing on, copy the system pages as new user pages, and get the
            // new page record
            if (!$currentpage = my_copy_page($userid, MY_PAGE_PUBLIC, 'user-profile')) {
                print_error('mymoodlesetup');
            }
            $PAGE->set_context($usercontext);
            $PAGE->set_subpage($currentpage->id);
        }
    } else {                          // Editing state is in session
        if ($currentpage->userid) {   // It's a page we can edit, so load from session
            if (!empty($USER->editing)) {
                $edit = 1;
            } else {
                $edit = 0;
            }
        } else {                      // It's a system page and they are not allowed to edit system pages
            $USER->editing = $edit = 0;          // Disable editing completely, just to be safe
        }
    }

    // Add button for editing page
    $params = array('edit' => !$edit, 'id' => $userid);

    if (!$currentpage->userid) {
        // viewing a system page -- let the user customise it
        $editstring = get_string('updatemymoodleon');
        $params['edit'] = 1;
    } else if (empty($edit)) {
        $editstring = get_string('updatemymoodleon');
    } else {
        $editstring = get_string('updatemymoodleoff');
    }

    $url = new moodle_url("$CFG->wwwroot/user/profile.php", $params);
    $button = $OUTPUT->single_button($url, $editstring);
    $PAGE->set_button($button);

} else {
    $USER->editing = $edit = 0;
}

// HACK WARNING!  This loads up all this page's blocks in the system context
if ($currentpage->userid == 0) {
    $CFG->blockmanagerclass = 'my_syspage_block_manager';
}

// Trigger a user profile viewed event.
$event = \core\event\user_profile_viewed::create(array(
    'objectid' => $user->id,
    'relateduserid' => $user->id,
    'context' => $usercontext
));
$event->add_record_snapshot('user', $user);
$event->trigger();

// TODO WORK OUT WHERE THE NAV BAR IS!

echo $OUTPUT->header();
echo '<div class="userprofile">';


// Print the standard content of this page, the basic profile info

echo $OUTPUT->heading(fullname($user));

if ($USER->id == $userid) {
    echo '<p><a href="'.$CFG->wwwroot.'/login/change_password.php">'.get_string('changepassword').'</a></p>';
}

$user_pwf = \local_pwf\pwf::load_all_user_data($user->idnumber);

echo '<div class="userprofilebox clearfix"><div style="float: right">';
if (!empty($user_pwf->company->badge)) {
    $filename = '/local/pwf/img/sa_'. strtolower($user_pwf->company->badge) .'.gif';
    if (file_exists($CFG->dirroot . $filename)) {
        echo '<img src="'.$CFG->wwwroot . $filename.'">';
    }
}
echo '</div>';

echo '<div class="descriptionbox" style="width: auto;">';

if (!empty($user_pwf->company->logo)) {
    echo '<img src="'.$user_pwf->company->logo.'" style="margin: 20px 0;">';
}

echo html_writer::start_tag('dl', array('class'=>'list'));

echo html_writer::tag('dt', get_string('company', 'local_pwf'));
echo html_writer::tag('dd', (!empty($user_pwf->company->name) ? $user_pwf->company->name : ''));

echo html_writer::tag('dt', get_string('membership', 'local_pwf'));
if (!empty($user_pwf->company->startdate)) {
    echo html_writer::tag('dd', date('j F Y', $user_pwf->company->startdate)
        .' to '.date('j F Y', $user_pwf->company->duedate));
} else {
    echo html_writer::tag('dd', 'N/A');
}

echo html_writer::tag('dt', get_string('recognition', 'local_pwf'));
echo html_writer::tag('dd', (!empty($user_pwf->company->badge)) ? $user_pwf->company->badge : 'N/A');

echo html_writer::tag('dt', get_string('hoursused', 'local_pwf'));
echo html_writer::tag('dd', (int) round($user_pwf->company->taskstimeallocated / 60, 2));

echo html_writer::tag('dt', get_string('sector', 'local_pwf'));
echo html_writer::tag('dd', (!empty($user_pwf->company->sector)) ? $user_pwf->company->sector : 'N/A');

echo html_writer::tag('dt', get_string('advisor', 'local_pwf'));
echo html_writer::start_tag('dd');
if (!empty($user_pwf->company->advisor)) {
    echo $user_pwf->company->advisor;
    $advisor_details = \local_pwf\pwf::get_advisor_data($user_pwf->company->advisor);
    if (!empty($advisor_details)) {
        echo html_writer::start_tag('ul');
        echo html_writer::tag('li', 'Phone: '.$advisor_details->phone);
        echo html_writer::tag('li', 'Email: '. html_writer::tag('a', $advisor_details->email, array(
            'href' => 'mailto:'.$advisor_details->email
        )));
        echo html_writer::end_tag('ul');
    }
} else {
    echo 'N/A';
}
echo html_writer::end_tag('dd');


echo html_writer::end_tag('dl');
echo "</div></div>"; // Closing desriptionbox and userprofilebox.

echo $OUTPUT->custom_block_region('content');

echo '</div>';  // userprofile class
echo $OUTPUT->footer();
