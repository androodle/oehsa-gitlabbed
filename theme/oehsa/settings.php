<?php

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

        // Logo file setting.
        $name = 'theme_oehsa/logo';
        $title = get_string('logo','theme_oehsa');
        $description = get_string('logodesc', 'theme_oehsa');
        //$default = '';
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
        
        // Favicon file setting.
        $name = 'theme_oehsa/favicon';
        $title = new lang_string('favicon', 'theme_oehsa');
        $description = new lang_string('favicondesc', 'theme_oehsa');
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'favicon', 0, array('accepted_types' => '.ico'));
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Banner header text setting
        $name = 'theme_oehsa/bannertext';
        $title = get_string('bannertext', 'theme_oehsa');
        $description = get_string('bannertextdesc', 'theme_oehsa');
        $default = '';
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Custom CSS file
        $name = 'theme_oehsa/customcss';
        $title = get_string('customcss', 'theme_oehsa');
        $description = get_string('customcssdesc', 'theme_oehsa');
        $default = '';
        $setting = new admin_setting_configtextarea($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Footnote setting
        $name = 'theme_oehsa/footnote';
        $title = get_string('footnote', 'theme_oehsa');
        $description = get_string('footnotedesc', 'theme_oehsa');
        $default = '';
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
}