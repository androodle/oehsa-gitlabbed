<?php
/**
* Theme version info
*
* @package    theme
* @subpackage androtheme
*
* Documentation: http://docs.moodle.org/dev/version.php
*/

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2014101300; // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2013050100; // Requires this Moodle version
$plugin->component = 'theme_androtheme'; // Type and name of this plugin
$plugin->maturity = MATURITY_STABLE;
$plugin->release = 'v1.2';
$plugin->dependencies = array(
    'theme_bootstrapbase'  => 2013050100,
);