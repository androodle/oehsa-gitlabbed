<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/*
 * @package    format
 * @subpackage tiles
 * @author     Greg Newton, Androgogic <greg.newton@androgogic.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  2014 Androgogic, Ltd.
 *
 * TODO: Description goes here
 */

require_once('../../../config.php');
require_once($CFG->dirroot . '/course/format/tiles/locallib.php');

require_login();

$courseid = required_param('courseid', PARAM_INT);
$sectionid = required_param('sectionid', PARAM_INT);

$context = context_course::instance($courseid);
$PAGE->set_context($context);
$PAGE->set_url('/format/tiles/introbackground.php');
$PAGE->set_heading($SITE->fullname);
$PAGE->set_pagelayout('course');
$PAGE->set_title(get_string('choose_intro_image', 'format_tiles'));
$PAGE->navbar->add(get_string('choose_intro_image', 'format_tiles'));

require_capability('format/tiles:changeintroductionbackground', $context);

require_once('./forms/chooseintroimage_form.php');

$form = new format_tiles_chooseintroimage_form(null, array());

$form_data = array("courseid" => $courseid, 'sectionid' => $sectionid);

$existing = $DB->get_record('format_tiles_tile_image', array('courseid' => $courseid, 'sectionid' => $sectionid));
if ($existing) {
    $form_data["repeat"] = $existing->bgrepeat;
}

if ($form->is_cancelled()) {
    // Go back to course if cancelled.
    redirect(new moodle_url($CFG->wwwroot . '/course/view.php', array('id' => $courseid)));
} else if ($formdata = $form->get_data()) {
    // Submitted
    $courseid = $formdata->courseid;
    $sectionid = $formdata->sectionid;
    $context = context_course::instance($courseid);

    $imagefile = $form->save_stored_file('imagefile', $context->id, 'format_tiles', 'section', $sectionid, '/', null, true);

    if ($existing) {
        // If the file has the same filename as the previous it will have been overwritten
        // by the upload, otherwise we need to clean up the old file.
        if ($existing->filename != $imagefile->get_filename()) {
            $fs = get_file_storage();
            $file = $fs->get_file($context->id, 'course', 'section', $sectionid,
                '/format_tiles/', $existing->filename);
            if ($file) {
                $file->delete();
            }
        }
        $existing->bgrepeat = $formdata->repeat;
        $existing->userid = $USER->id;
        $existing->timemodified = time();
        $existing->filename = $imagefile->get_filename();
        $DB->update_record('format_tiles_tile_image', $existing);
    } else {
        $new_format_tiles_tile_image = new stdClass();
        $new_format_tiles_tile_image->courseid = $courseid;
        $new_format_tiles_tile_image->sectionid = $sectionid;
        $new_format_tiles_tile_image->bgrepeat = $formdata->repeat;
        $new_format_tiles_tile_image->userid = $USER->id;
        $new_format_tiles_tile_image->filename = $imagefile->get_filename();
        $new_format_tiles_tile_image->timecreated = time();
        $new_format_tiles_tile_image->timemodified = time();
        $format_tiles_tile_image_id = $DB->insert_record('format_tiles_tile_image', $new_format_tiles_tile_image);
    }

    redirect(new moodle_url($CFG->wwwroot . '/course/view.php', array('id' => $courseid)));
}
$form->set_data($form_data);

// Output content.
echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('choose_intro_image', 'format_tiles'), 3, 'main');

$form->display();
echo $OUTPUT->footer();
