<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/*
 * @package    format
 * @subpackage tiles
 * @author     Greg Newton, Androgogic <greg.newton@androgogic.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright  2014-15 Androgogic, Ltd.
 *
 * Strings for component 'format_tiles', language 'en'
 */

$string['pluginname'] = 'Tile format';

$string['all_sections_default'] = 'Default for using all sections view';
$string['all_sections_default_desc'] = 'Default state for all sections view in a course.';
$string['allow_all_sections_home'] = 'Allow all sections homepage';
$string['allow_all_sections_home_desc'] = 'Allow the all sections veiw to be used in the place of the tiles homepage.';
$string['allow_all_sections_tile'] = 'Allow all sections tile';
$string['allow_all_sections_tile_desc'] = 'Allow an all sections tile to be show in addition to the tab.';
$string['allow_all_sections_view'] = 'Allow all sections view';
$string['allow_all_sections_view_desc'] = 'Allows all sections view to be used.';
$string['allow_bottom_tabs'] = 'Allow bottom tabs';
$string['allow_bottom_tabs_desc'] = 'Allow users to choose to have tabs to be shown at the bottom of a page, as well as the top.';
$string['allow_custom_tiles'] = 'Allow custom tile backgrounds';
$string['allow_custom_tiles_desc'] = 'Allow users to specify custom backgrounds for tiles.';
$string['allow_intro_backgrounds'] = 'Allow custom introduction backgrounds';
$string['allow_intro_backgrounds_desc'] = 'Allow users to specify custom backgrounds for the introductory section.';
$string['allow_title_tabs'] = 'Allow titles in tabs.';
$string['allow_title_tabs_desc'] = 'Allow users to choose whether a section title can be shown in a tab instead of the number.';
$string['allsect_show_both'] = 'Show as tile and tab';
$string['allsect_show_neither'] = 'Do not show';
$string['allsect_show_tab_only'] = 'Show as tab';
$string['allsections'] = 'All sections';
$string['allsectionsview'] = 'All sections view';
$string['background_not_set'] = 'No custom background set.';
$string['background_preview'] = 'Background preview';
$string['background_set'] = 'Custom background set by {$a->firstname} {$a->lastname} on {$a->date}.';
$string['bottom_tabs'] = 'Bottom tabs';
$string['bottom_tabs_default'] = 'Default for showing bottom tabs';
$string['bottom_tabs_default_desc'] = 'Default visibility for bottom tab. Can be overridden at the course level.';
$string['change_background'] = 'Change background';
$string['choose_image'] = 'Choose Background Image';
$string['choose_image_help'] = 'Choose an image to be used as the background of a tile. Be aware that tiles might not always be the same size on other people\'s screens.';
$string['choose_intro_image'] = 'Choose introduction background';
$string['custom_tiles'] = 'Custom Backgrounds';
$string['hiddensectionstileshown'] = 'Hidden section tiles are shown';
$string['hide'] = 'Hide';
$string['hidefromothers'] = 'Hide Topic';
$string['homepage_layout'] = 'Home page layout';
$string['homepage_layout_all_sections'] = 'All sections';
$string['homepage_layout_default'] = 'Default homepage layout';
$string['homepage_layout_default_desc'] = 'This is the layout that will be used by default in new courses.';
$string['homepage_layout_help'] = 'Format to use for the home page.';
$string['homepage_layout_tiles'] = 'Tiles';
$string['max_title_length'] = 'Max title length';
$string['max_title_length_desc'] = 'Titles shown in tabs will be trimmed to this length. A value of 0 will mean titles are not trimmed.';
$string['remove_background'] = 'Remove background';
$string['remove_warning'] = 'Do you really wish to remove the custom background?';
$string['repeat'] = 'Repeat';
$string['repeat_both'] = 'Vertical and horizontal';
$string['repeat_horizontal'] = 'Horizontal';
$string['repeat_none'] = 'No repeat';
$string['repeat_vertical'] = 'Vertical';
$string['section0name'] = 'General';
$string['sectionname'] = 'Topic';
$string['sectionnumber'] = 'Section Number';
$string['sectiontitle'] = 'Section Title';
$string['set_background'] = 'Set background';
$string['show'] = 'Show';
$string['showallsections'] = 'Show all sections tabs and tiles';
$string['showallsections_help'] = 'Show tabs and tiles that allow users to view all sections at once';
$string['showbottomtabs'] = 'Show tabs at the bottom';
$string['showbottomtabs_help'] = 'Show tabs at the bottom of section pages, in addition to the top';
$string['showfromothers'] = 'Show Topic';
$string['showtitlesintabs'] = 'Show titles in tabs';
$string['showtitlesintabs_help'] = 'Should titles be shown in tabs rather than section numbers.';
$string['textintabs'] = 'Show text in tabs';
$string['textintabs_help'] = 'Show section headings in tabs';
$string['tile_preview'] = 'Tile Preview';
$string['tiles:changeintroductionbackground'] = 'Change the background of the introductory section';
$string['tiles:changetilebackground'] = 'Change the background of a tile';
$string['title_tabs'] = 'Titles in tabs';
$string['title_tabs_default'] = 'Default for showing titles in tabs';
$string['title_tabs_default_desc'] = 'Default state for showing section titles in tabs. Can be overridden at the course level.';
$string['use'] = 'Use';
